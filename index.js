/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printPersonalInformation() {

		let fullName = prompt("Enter your Full Name.");
		let age = prompt("Enter your Age.");
		let location = prompt("Enter your Location.");

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old.");
		console.log("You live in " + location);

	}

	printPersonalInformation();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function showMusician() {

		let faveMusician1 = "One Direction";
		let faveMusician2 = "Day 6";
		let faveMusician3 = "Regina Specter";
		let faveMusician4 = "Ebe Dancel";
		let faveMusician5 = "My Chemical Romance";

		console.log("1. " + faveMusician1);
		console.log("2. " + faveMusician2);
		console.log("3. " + faveMusician3);
		console.log("4. " + faveMusician4);
		console.log("5. " + faveMusician5);
	}

	showMusician();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function showTopFiveMovies()
	{
		let faveMovie1 = "Batman Begins";
		let faveMovie2 = "The Dark Knight";
		let faveMovie3 = "The Dark Knight Rises";
		let faveMovie4 = "Justice League: Snyder's Cut";
		let faveMovie5 = "Three Idiots";

		console.log("1. " + faveMovie1);
		console.log("Rotten Tomatoes Rating: 84%");
		console.log("2. " + faveMovie2);
		console.log("Rotten Tomatoes Rating: 92%");
		console.log("3. " + faveMovie3);
		console.log("Rotten Tomatoes Rating: 90%");
		console.log("4. " + faveMovie4);
		console.log("Rotten Tomatoes Rating: 86%");
		console.log("5. " + faveMovie5);
		console.log("Rotten Tomatoes Rating: 95%");
	}

	showTopFiveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers()
{
	//let printFriends() = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name: "); 
		let friend2 = prompt("Enter your second friend's name: "); 
		let friend3 = prompt("Enter your third friend's name: ");

		console.log("You are friends with: " + friend1);
		// console.log(friend1); 
		console.log("You are friends with: " + friend2); 
		console.log("You are friends with: " + friend3); 
};

printUsers()
// console.log(friend1);
// console.log(friend2);
